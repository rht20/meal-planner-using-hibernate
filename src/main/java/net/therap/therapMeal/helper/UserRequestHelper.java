package net.therap.therapMeal.helper;

import net.therap.therapMeal.dao.*;
import net.therap.therapMeal.domain.*;
import net.therap.therapMeal.service.*;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class UserRequestHelper {

    private ItemDao itemDao;
    private MealTypeDao mealTypeDao;
    private MenuDao menuDao;

    private ItemService itemService;
    private MealService mealService;
    private MenuService menuService;

    private PrintHelper printHelper;
    private InputHelper inputHelper;

    public UserRequestHelper(EntityManager em) {
        this.itemDao = new ItemDao(em);
        this.mealTypeDao = new MealTypeDao(em);
        this.menuDao = new MenuDao(em);

        this.itemService = new ItemService(em);
        this.mealService = new MealService(em);
        this.menuService = new MenuService(em);

        this.printHelper = new PrintHelper();
        this.inputHelper = new InputHelper();
    }

    public void serveUser() {
        while (true) {
            printHelper.printMainMenu();

            if (chooseMainMenuOption()) {
                break;
            }
        }
    }

    public boolean chooseMainMenuOption() {
        printHelper.printString("Enter option id: ");
        int option = inputHelper.inputInteger();

        switch (option) {
            case 1:
                List<Item> itemList = itemService.viewItems();
                printHelper.printItemList(itemList);
                break;
            case 2:
                List<Meal> mealList = mealService.viewMealTypes();
                printHelper.printMealList(mealList);
                break;
            case 3:
                Meal meal = getMealInfo();
                Day day = getDayInfo();
                Menu menu = menuService.viewMenu(meal, day);
                printHelper.printMenu(menu);
                break;
            case 4:
                printHelper.printString("Enter item name: ");
                String itemName = inputHelper.inputString();
                itemService.addNewItem(itemName);
                break;
            case 5:
                Item item = getItemInfo();
                itemService.removeItem(item);
                break;
            case 6:
                printHelper.printString("Enter type name: ");
                String typeName = inputHelper.inputString();
                mealService.addNewMealType(typeName);
                break;
            case 7:
                meal = getMealInfo();
                mealService.removeMealType(meal);
                break;
            case 8:
                meal = getMealInfo();
                day = getDayInfo();
                item = getItemInfo(meal, day, false);
                menuService.addItemToMenu(meal, day, item);
                break;
            case 9:
                meal = getMealInfo();
                day = getDayInfo();
                item = getItemInfo(meal, day, true);
                menuService.removeItemFromMenu(meal, day, item);
                break;
        }

        return (option == 10);
    }

    public Item getItemInfo() {
        List<Item> itemList = itemDao.getItems();
        printHelper.printItemList(itemList);

        printHelper.printString("Enter item id: ");
        int id = inputHelper.inputInteger();

        return itemList.get(id - 1);
    }

    public Meal getMealInfo() {
        List<Meal> mealList = mealTypeDao.getMealTypes();

        printHelper.printMealList(mealList);
        printHelper.printString("Enter meal id: ");
        int id = inputHelper.inputInteger();

        return mealList.get(id - 1);
    }

    public Item getItemInfo(Meal meal, Day day, boolean flag) {
        List<Item> itemList;

        if (flag) {
            itemList = menuDao.getMenu(meal, day).getItemList();
        } else {
            itemList = itemDao.getItems();
        }

        printHelper.printItemList(itemList);
        printHelper.printString("Enter item id: ");
        int id = inputHelper.inputInteger();

        return itemList.get(id - 1);
    }

    public Day getDayInfo() {
        printHelper.printDayList();
        printHelper.printString("Enter day id: ");
        int id = inputHelper.inputInteger();

        return Day.values()[id - 1];
    }
}
