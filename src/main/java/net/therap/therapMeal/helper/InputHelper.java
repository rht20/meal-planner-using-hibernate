package net.therap.therapMeal.helper;

import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class InputHelper {

    private Scanner scanner;

    public InputHelper() {
        this.scanner = new Scanner(System.in);
    }

    public int inputInteger() {
        return scanner.nextInt();
    }

    public String inputString() {
        String str;

        while (true) {
            str =  scanner.nextLine();
            if (!str.equals("")) {
                break;
            }
        }

        return str;
    }
}
