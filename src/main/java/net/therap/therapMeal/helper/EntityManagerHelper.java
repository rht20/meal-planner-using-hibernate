package net.therap.therapMeal.helper;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author rakibul.hasan
 * @since 2/23/20
 */
public class EntityManagerHelper {

    private EntityManagerFactory emf;
    private EntityManager em;

    public EntityManagerHelper() {
        this.emf = Persistence.createEntityManagerFactory("persistence");
        this.em = emf.createEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public void close() {
        em.close();
        emf.close();
    }
}
