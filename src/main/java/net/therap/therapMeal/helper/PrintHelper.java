package net.therap.therapMeal.helper;

import net.therap.therapMeal.domain.*;

import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class PrintHelper {

    public void printItemList(List<Item> itemList) {
        System.out.println("Available items:");
        for (int i = 0; i < itemList.size(); i++) {
            System.out.println((i+1) + ". " + itemList.get(i).getName());
        }
        System.out.println();
    }

    public void printMealList(List<Meal> mealList) {
        System.out.println("Available meal types:");
        for (int i = 0; i < mealList.size(); i++) {
            System.out.println((i+1) + ". " + mealList.get(i).getName());
        }
        System.out.println();
    }

    public void printMenu(Menu menu) {
        System.out.println("Meal type: " + menu.getMeal().getName());
        System.out.println("Day: " + menu.getDay());

        System.out.print("Menu: ");
        if (menu.getItemList().isEmpty()) {
            System.out.println("Empty");
        } else {
            for (int i = 0; i < menu.getItemList().size(); i++) {
                if (i > 0) {
                    System.out.print(", ");
                }
                System.out.print(menu.getItemList().get(i).getName());
            }
        }

        System.out.println();
    }

    public void printDayList() {
        System.out.println("Weekly days:");
        for (int i = 0; i < Day.values().length; i++) {
            System.out.println((i+1) + ". " + Day.values()[i]);
        }
        System.out.println();
    }

    public void printMainMenu() {
        System.out.println("Select option:");
        System.out.println("1. View items");
        System.out.println("2. View meal types");
        System.out.println("3. View menu");
        System.out.println("4. Add new item");
        System.out.println("5. Remove item");
        System.out.println("6. Add new meal type");
        System.out.println("7. Remove meal type");
        System.out.println("8. Add item to menu");
        System.out.println("9. Remove item from menu");
        System.out.println("10. Exit");
        System.out.println();
    }

    public void printString(String str) {
        System.out.print(str);
    }
}
