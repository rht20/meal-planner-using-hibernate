package net.therap.therapMeal.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
@Entity
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    private Meal meal;

    private Day day;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Item> itemList;

    public Menu() {
        this.itemList = new ArrayList<>();
    }

    public Menu(Meal meal, Day day) {
        this.meal = meal;
        this.day = day;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public int getId() {
        return id;
    }

    public Meal getMeal() {
        return meal;
    }

    public Day getDay() {
        return day;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", meal=" + meal +
                ", day=" + day +
                ", itemList=" + itemList +
                '}';
    }
}
