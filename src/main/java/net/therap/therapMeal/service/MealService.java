package net.therap.therapMeal.service;

import net.therap.therapMeal.dao.MealTypeDao;
import net.therap.therapMeal.dao.MenuDao;
import net.therap.therapMeal.domain.Meal;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/11/20
 */
public class MealService {

    private MealTypeDao mealTypeDao;
    private MenuDao menuDao;

    public MealService(EntityManager em) {
        this.mealTypeDao = new MealTypeDao(em);
        this.menuDao = new MenuDao(em);
    }

    public List<Meal> viewMealTypes() {
        List<Meal> mealList = mealTypeDao.getMealTypes();
        return mealList;
    }

    public void addNewMealType(String typeName) {
        Meal meal = new Meal();
        meal.setName(typeName);
        mealTypeDao.insertMealType(meal);
    }

    public void removeMealType(Meal meal) {
        menuDao.removeByMealType(meal);
        mealTypeDao.removeMealType(meal);
    }
}
