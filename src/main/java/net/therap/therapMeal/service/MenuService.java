package net.therap.therapMeal.service;

import net.therap.therapMeal.dao.MenuDao;
import net.therap.therapMeal.domain.Day;
import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.domain.Meal;
import net.therap.therapMeal.domain.Menu;

import javax.persistence.EntityManager;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class MenuService {

    private MenuDao menuDao;

    public MenuService(EntityManager em) {
        this.menuDao = new MenuDao(em);
    }

    public Menu viewMenu(Meal meal, Day day) {
        Menu menu = menuDao.getMenu(meal, day);
        return menu;
    }

    public void addItemToMenu(Meal meal, Day day, Item item) {
        menuDao.insertMenu(meal, day, item);
    }

    public void removeItemFromMenu(Meal meal, Day day, Item item) {
        menuDao.removeMenuItem(meal, day, item);
    }
}
