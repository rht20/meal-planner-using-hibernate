package net.therap.therapMeal.service;

import net.therap.therapMeal.dao.ItemDao;
import net.therap.therapMeal.dao.MenuDao;
import net.therap.therapMeal.domain.Item;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class ItemService {

    private ItemDao itemDao;
    private MenuDao menuDao;

    public ItemService(EntityManager em) {
        this.itemDao = new ItemDao(em);
        this.menuDao = new MenuDao(em);
    }

    public List<Item> viewItems() {
        List<Item> itemList = itemDao.getItems();
        return itemList;
    }

    public void addNewItem(String itemName) {
        Item item = new Item();
        item.setName(itemName);
        itemDao.insertItem(item);
    }

    public void removeItem(Item item) {
        menuDao.removeByItem(item);
        itemDao.removeItem(item);
    }
}
