package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.Item;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class ItemDao {

    private EntityManager em;

    public ItemDao(EntityManager em) {
        this.em = em;
    }

    public List<Item> getItems() {
        List<Item> itemList = em.createQuery("FROM Item").getResultList();
        return itemList;
    }

    public void insertItem(Item item) {
        em.getTransaction().begin();
        em.persist(item);
        em.getTransaction().commit();
    }

    public void removeItem(Item item) {
        em.getTransaction().begin();
        em.remove(em.contains(item) ? item : em.merge(item));
        em.getTransaction().commit();
    }
}
