package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.Meal;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class MealTypeDao {

    private EntityManager em;

    public MealTypeDao(EntityManager em) {
        this.em = em;
    }

    public List<Meal> getMealTypes() {
        List<Meal> mealList = em.createQuery("FROM Meal").getResultList();
        return mealList;
    }

    public void insertMealType(Meal meal) {
        em.getTransaction().begin();
        em.persist(meal);
        em.getTransaction().commit();
    }

    public void removeMealType(Meal meal) {
        em.getTransaction().begin();
        em.remove(em.contains(meal) ? meal : em.merge(meal));
        em.getTransaction().commit();
    }
}
