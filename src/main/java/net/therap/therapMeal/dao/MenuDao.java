package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.Day;
import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.domain.Meal;
import net.therap.therapMeal.domain.Menu;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class MenuDao {

    private EntityManager em;

    public MenuDao(EntityManager em) {
        this.em = em;
    }

    public Menu getMenu(Meal meal, Day day) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.meal = :meal AND M.day = :day", Menu.class)
                .setParameter("meal", meal)
                .setParameter("day", day);

        List<Menu> menuList = query.getResultList();
        Menu menu = menuList.isEmpty() ? new Menu(meal, day) : menuList.get(0);

        return menu;
    }

    public void insertMenu(Meal meal, Day day, Item item) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.meal = :meal AND M.day = :day", Menu.class)
        .setParameter("meal", meal)
        .setParameter("day", day);

        List<Menu> menuList = query.getResultList();
        Menu menu = menuList.isEmpty() ? new Menu(meal, day) : menuList.get(0);
        menu.getItemList().add(item);

        em.getTransaction().begin();
        em.persist(menu);
        em.getTransaction().commit();
    }

    public void removeByItem(Item item) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M", Menu.class);
        List<Menu> menuList = query.getResultList();

        em.getTransaction().begin();
        for (Menu menu : menuList) {
            if (menu.getItemList().contains(em.contains(item) ? item : em.merge(item))) {
                menu.getItemList().remove(em.contains(item) ? item : em.merge(item));
            }
        }
        em.getTransaction().commit();
    }

    public void removeByMealType(Meal meal) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.meal = :meal", Menu.class)
        .setParameter("meal", meal);
        List<Menu> menuList = query.getResultList();

        em.getTransaction().begin();
        for (Menu menu : menuList) {
            em.remove(menu);
        }
        em.getTransaction().commit();
    }

    public void removeMenuItem(Meal meal, Day day, Item item) {
        TypedQuery<Menu> query = em.createQuery("SELECT M FROM Menu AS M WHERE M.meal = :meal AND M.day = :day", Menu.class)
        .setParameter("meal", meal)
        .setParameter("day", day);

        Menu menu = query.getSingleResult();

        em.getTransaction().begin();
        menu.getItemList().remove(em.contains(item) ? item : em.merge(item));
        em.persist(menu);
        em.getTransaction().commit();
    }
}
