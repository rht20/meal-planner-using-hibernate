package net.therap.therapMeal.app;

import net.therap.therapMeal.helper.EntityManagerHelper;
import net.therap.therapMeal.helper.UserRequestHelper;

/**
 * @author rakibul.hasan
 * @since 2/11/20
 */
public class Main {

    public static void main(String[] args) {
        EntityManagerHelper emh = new EntityManagerHelper();
        UserRequestHelper urh = new UserRequestHelper(emh.getEntityManager());
        urh.serveUser();
        emh.close();
    }
}
